import itertools

lst = [1,2,3,4,5]
sum_list = itertools.accumulate(lst)
print(list(sum_list))

lst2 = ['1','2','3','4','5']
chain_list = itertools.chain(lst, lst2)
print(list(chain_list))

lst3 = ['E','S','T','A','B']
show = [1,1,1,0,0]
compressed_list = itertools.compress(lst3, show)
print(list(compressed_list))