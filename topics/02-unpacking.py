# Unpacking

tup = (1,2,3,4,5)
lst = [1,2,3,4,5]
string = "hello"
dic = {"a":1,"b":2}
coords = [4,5]

a,b,c,d,e = tup
print(a,b,c,d,e)

a1,a2,a3,a4,a5 = lst
print(a4)

b1,b2,b3,b4,b5 = string
print(b1)

# c1,c2 = dic
# print(c1) # a

# c1,c2 = dic.values()
# print(c1) # 1

c1,c2 = dic.items()
print(c1) # ('a' ,1)