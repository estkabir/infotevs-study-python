import asyncio
import time

async def main(second: int) -> None:
    print("init main " + str(second))
    task = asyncio.create_task(foo('text', second))
    await asyncio.sleep(second-1)
    # await task
    print("finish main")

async def foo(text:str, second: int) -> None:
    print("init foo " + text )
    await asyncio.sleep(second)
    print("finish foo")

asyncio.run(main(2))
print('end file')