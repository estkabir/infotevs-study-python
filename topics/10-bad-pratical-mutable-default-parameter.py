
def append_number(num, numbers=[]):
    numbers.append(num)
    print(num)
    print(numbers)
    return numbers

def append_number_2(num, numbers = None ):
    if (numbers == None):
        numbers = list()
    numbers.append(num)
    print(num)
    print(numbers)
    return numbers


x = append_number_2(1)
y = append_number_2(2)
z = append_number_2(3)

print(x == y == z)
