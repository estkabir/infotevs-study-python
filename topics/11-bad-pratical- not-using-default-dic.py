counts = {}
numbers = [1,2,3,4,2,3,4,1,5]
for key in numbers:
    if key not in counts:
        counts[key] = 0
    counts[key] += 1

print(counts)

from collections import defaultdict

counts = defaultdict(lambda: 0)

for key in numbers:
    counts[key] += 1

print(counts)