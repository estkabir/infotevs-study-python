# Comprehensions

def soma(i):
    return i+1

soma_2 = lambda x: x+1

x = [ (lambda x: x+1)(i) for i in range(100) if i % 2 == 0]

y = (i for i in "hello")

words = "Hello my name is Estevão"
z = {char: words.count(char) for char in set(words)}

# print(x)
# print(list(y))
# print(tuple(y))
print(z)