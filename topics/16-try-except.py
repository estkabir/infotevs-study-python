

try:
    f = open('text.txt')
except FileNotFoundError as e:
    print(e)
except Exception:
    print('Error!')
else: #se try não der error
    print(f.read())
    f.close()
finally:
    print('Finalmente executando finally')