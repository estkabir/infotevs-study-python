import sys

x = [1,2,3,4,5,6,7,8,9,10]

# print(sys.getsizeof(x))
# print(sys.getsizeof(range(1,11)))
# for i in x:
#     print(i)

y = map(lambda i: i**2, x)
# print(y)
# print(sys.getsizeof(y))
# print(list(y))
# print(sys.getsizeof(list(y)))

# print(next(y))
# print(y.__next__())
# print(next(y))
# print(next(y))
# print('For loop start')
# for i in y:
#     print(i)

# while True:
#     try:
#         value = next(y)
#         print(value)
#     except StopIteration:
#         print('done')
#         break

# k = range(1,11)
# print(next(iter(k)))
# for i in iter(k):
#     print(i)

def gen(n):
    for i in range(n):
        yield n

for i in gen(5):
    print(i)