import threading
import time

def func(y):
    print('ran', 4)
    time.sleep(1)
    print('running')
    time.sleep(1)
    print('now done')

x = threading.Thread(target=func, args=(4,))
x.start()
print(threading.active_count())
time.sleep(1)
print('finally')